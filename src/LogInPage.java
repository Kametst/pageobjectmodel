import io.github.bonigarcia.wdm.WebDriverManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class LogInPage {
    public static WebDriver driver;
    public static void main(String[] args) throws IOException, ParseException {

        FileInputStream fileInput = new FileInputStream("/Users/kame/IdeaProjects/PageFactDemo/src/Data.Properties");
        Properties prop = new Properties();
        prop.load(fileInput);
        if (prop.getProperty("browser").equals("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        }


        JSONParser p = new JSONParser();
        Object obj = p.parse(new FileReader("/Users/kame/IdeaProjects/PageFactDemo/src/Data.json"));
        JSONObject jObj = (JSONObject) obj;
        JSONArray array = (JSONArray) jObj.get("SignInData");
        JSONObject userDetail = (JSONObject) array.get(0);
        driver.get(prop.getProperty("url"));
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        //HomePage
        HomePage home = new HomePage(driver);
        home.signIn();

        //Call Signup Page
        SignUp signup = new SignUp(driver);
        signup.EnterEmail2((String) userDetail.get("email"));
        signup.EnterPassword((String) userDetail.get("passwd"));
        signup.SubmitlogIn();

        //Call Dashboard
        Dshbrd db = new Dshbrd(driver);
        db.WomenClick();
        db.MoreClick();

        //call cart
        AddCart ac = new AddCart(driver);
        ac.addToCart();

    }
}
