import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddCart extends PageInit{

    public AddCart(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"add_to_cart\"]/button")
    WebElement addCart;

    public void addToCart(){
        addCart.click();
    }
}
