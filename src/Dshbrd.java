import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.interactions.Actions;

public class Dshbrd extends PageInit{
    public WebDriver driver;
    public Dshbrd(WebDriver driver){
        super(driver);
        this.driver=driver;
    }

    @FindBy(linkText ="Women")
    WebElement women;

     @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[2]/div/div[1]/div/a[1]/img")
    WebElement img;

     @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[2]/div/div[2]/div[2]/a[2]")
    WebElement more;

     public void WomenClick(){
         women.click();
     }

     public void MoreClick(){
         Actions act = new Actions(driver);
         act.moveToElement(img).moveToElement(more).click().perform();
     }
}
