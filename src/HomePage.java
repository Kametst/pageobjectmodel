import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageInit{
    public HomePage(WebDriver driver){
        super(driver);
    }

    @FindBy(linkText = "Sign in")
    WebElement signin;

    public void signIn(){
        signin.click();

    }

}
