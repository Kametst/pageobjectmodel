import io.github.bonigarcia.wdm.WebDriverManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SignInPage {

    public static WebDriver driver;
    public static void main(String[] args) throws IOException, ParseException {

        FileInputStream fileInput = new FileInputStream("/Users/kame/IdeaProjects/PageFactDemo/src/Data.Properties");
        Properties prop = new Properties();
        prop.load(fileInput);
        if(prop.getProperty("browser").equals("chrome")){
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        }
        else {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        }


        JSONParser p= new JSONParser();
        Object obj=p.parse(new FileReader("/Users/kame/IdeaProjects/PageFactDemo/src/Data.json"));
        JSONObject jObj =  (JSONObject)obj;
        JSONArray array=(JSONArray)jObj.get("SignUpData");
        JSONObject userDetail = (JSONObject) array.get(0);
        driver.get(prop.getProperty("url"));
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        HomePage home = new HomePage(driver);
        home.signIn();

        SignUp signup = new SignUp(driver);
        signup.EnterEmail((String) userDetail.get("email_create"));
        signup.SubmitCreate();

        UserRegPage urp = new UserRegPage(driver);
        urp.checkgndr();
        urp.firstName((String) userDetail.get("customer_firstname"));
        urp.lastName((String) userDetail.get("customer_lastname"));
        urp.password((String) userDetail.get("passwd"));
        urp.dayss();
        urp.monthh();
        urp.year();
        urp.company((String) userDetail.get("company"));
        urp.address((String) userDetail.get("address1"));
        urp.city((String) userDetail.get("city"));
        urp.stat();
        urp.pscode((String) userDetail.get("postcode"));
        urp.phnno((String) userDetail.get("phone_mobile"));
        urp.submitdet();
    }

}
