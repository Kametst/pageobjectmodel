import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUp extends PageInit{
    public SignUp(WebDriver driver){
                super(driver);
            }

    @FindBy(id="email_create")
    WebElement email1;

    @FindBy(id="SubmitCreate")
    WebElement SubmitButtonEmail;

    @FindBy(id="email")
    WebElement email2;

    @FindBy(id="passwd")
    WebElement password;

    @FindBy(id="SubmitLogin")
    WebElement SubmitButtonSignIn;


    public void EnterEmail(String email){
        email1.sendKeys(email);
    }

    public void SubmitCreate(){
        SubmitButtonEmail.click();
    }



    public void EnterEmail2(String email){
        email2.sendKeys(email);
    }

    public void EnterPassword(String passwd){
        password.sendKeys(passwd);
    }

    public void SubmitlogIn(){
        SubmitButtonSignIn.click();
    }
}
